# Budget Tracker

By Azizov Ramir
A simple Web application or tracking your finances

## Description

Budget Tracker is a web application for tracking your finances and managing your budget. With this app, you can easily record your income and expenses, track your budget, and create financial plans for the future.
## Technologies Used
+ Node.js
+ Express.js
+ MongoDB
+ React.js
+ Redux
+ Bootstrap



## Installation

1. Clone the repository from GitHub
2. Navigate to the application directory:
3. Install the dependencies
4. Run the application:
```
git clone https://github.com/azizoram/budget-tracker.git
cd budget-tracker
npm install
npm start
```

## Copyright

Copyright (c) 2023 Ramir Azizov.

This project is licensed under the MIT License - see the LICENSE file for details.
